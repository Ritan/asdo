# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from organization.forms import *
from django.contrib import admin
from organization.views import MaterialListView, MaterialView, TestListView, TestView, StatListView
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'organization.views.home', name='home'),

    url(r'^test/$', TestListView.as_view(), name='test'),
	
    url(r'^test/university/(?P<university_id>\d+)/$', TestListView.as_view(), name='test_university'),
    url(r'^test/university/(?P<university_id>\d+)/department/(?P<department_id>\d+)/$', TestListView.as_view(), name='test_department'),
    url(r'^test/university/(?P<university_id>\d+)/department/(?P<department_id>\d+)/subject/(?P<subject_id>\d+)/$', TestListView.as_view(), name='test_subject'),
    url(r'^test/university/(?P<university_id>\d+)/department/(?P<department_id>\d+)/subject/(?P<subject_id>\d+)/lector/(?P<lector_id>\d+)/$', TestListView.as_view(), name='test_lector'),
    url(r'^test/university/(?P<university_id>\d+)/department/(?P<department_id>\d+)/subject/(?P<subject_id>\d+)/lector/(?P<lector_id>\d+)/(?P<test_id>\d+)/$', TestView.as_view(), name='test_view'),
# добавление новго теста
    url(r'^add_education_test/$', 'organization.views.add_education_test', name='add_education_test'),
# редактирование вопроса к тесту
    url(r'^test/(?P<test_id>\d+)/question/(?P<question_id>\d+)/$', 'organization.views.question_view', name='question_view'),
# удаление вопроса из теста
    url(r'^remove_test_question/(?P<question_id>\d+)/$', 'organization.views.remove_test_question', name='remove_test_question'),
# публикация вопроса в тесте
    url(r'^toggle_pub_test_question/(?P<question_id>\d+)/$', 'organization.views.toggle_pub_test_question', name='toggle_pub_test_question'),
# публикация теста
    url(r'^toggle_pub_education_test/(?P<test_id>\d+)/$', 'organization.views.toggle_pub_education_test', name='toggle_pub_education_test'),
	url(r'^add_education_discipline/(?P<discipline_id>\d+)/$', TestListView.as_view(), name='test'),


    # прохождение теста
# запрос на инициализацию теста
    url(r'^ajax/init_test/$', 'organization.views.init_test', name='init_test'),
# запрос на получение следующего вопроса
    url(r'^ajax/get_next_question/$', 'organization.views.get_next_question', name='get_next_question'),
# сохранение ответа на вопрос
    url(r'^ajax/save_question/$', 'organization.views.save_question', name='save_question'),
# аварийное завершение теста
    url(r'^ajax/force_finish_test/$', 'organization.views.force_finish_test', name='force_finish_test'),


# запрос на получение нового id сессии для прохождения теста
    url(r'^ajax/get_new_uuid/$', 'organization.views.get_new_uuid', name='get_new_uuid'),




    url(r'^material/$', MaterialListView.as_view(), name='material'),
    url(r'^material/university/(?P<university_id>\d+)/$', MaterialListView.as_view(), name='material_university'),
    url(r'^material/university/(?P<university_id>\d+)/department/(?P<department_id>\d+)/$', MaterialListView.as_view(), name='material_department'),
    url(r'^material/university/(?P<university_id>\d+)/department/(?P<department_id>\d+)/subject/(?P<subject_id>\d+)/$', MaterialListView.as_view(), name='material_subject'),
    url(r'^material/university/(?P<university_id>\d+)/department/(?P<department_id>\d+)/subject/(?P<subject_id>\d+)/lector/(?P<lector_id>\d+)/$', MaterialListView.as_view(), name='material_lector'),
    url(r'^material/university/(?P<university_id>\d+)/department/(?P<department_id>\d+)/subject/(?P<subject_id>\d+)/lector/(?P<lector_id>\d+)/(?P<material_id>\d+)/$', MaterialView.as_view(), name='material_view'),


    url(r'^stat/$', StatListView.as_view(), name='stat'),
    url(r'^stat/university/(?P<university_id>\d+)/$', StatListView.as_view(), name='stat_university'),
    url(r'^stat/university/(?P<university_id>\d+)/department/(?P<department_id>\d+)/$', StatListView.as_view(), name='stat_department'),
    url(r'^stat/university/(?P<university_id>\d+)/department/(?P<department_id>\d+)/subject/(?P<subject_id>\d+)/$', StatListView.as_view(), name='stat_subject'),
    url(r'^stat/university/(?P<university_id>\d+)/department/(?P<department_id>\d+)/subject/(?P<subject_id>\d+)/lector/(?P<lector_id>\d+)/$', StatListView.as_view(), name='stat_lector'),


# добавление новго учебного материала
    url(r'^add_education_material/$', 'organization.views.add_education_material', name='add_education_material'),
# включение/выключени публикации материала
    url(r'^toggle_pub_education_material/(?P<material_id>\d+)/$', 'organization.views.toggle_pub_education_material', name='toggle_pub_education_material'),
# удаление материала вместе со всеми файлами
    url(r'^remove_education_material/(?P<material_id>\d+)/$', 'organization.views.remove_education_material', name='remove_education_material'),
# удаление файла в учебном материале
    url(r'^remove_education_material_file/(?P<file_id>\d+)/$', 'organization.views.remove_education_material_file', name='remove_education_material_file'),

# для меню
    url(r'^ajax/get_university_list/$', 'organization.views.get_university_list', name='get_university_list'),
    url(r'^ajax/get_department_list/$', 'organization.views.get_department_list', name='get_department_list'),
    url(r'^ajax/get_subject_list/$', 'organization.views.get_subject_list', name='get_subject_list'),
    url(r'^ajax/get_lector_list/$', 'organization.views.get_lector_list', name='get_lector_list'),



    url(r'^accounts/registration/$', 'organization.registration.registration', name='registration'),
    url(r'^accounts/registration/success/$', 'organization.registration.registration_success', name='registration_success'),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'authentication_form': UserAuthenticationForm}, name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout_then_login', name='logout'),
    url(r'^accounts/password_reset/$', 'django.contrib.auth.views.password_reset', {'post_reset_redirect':'/accounts/password_reset_done/', 'password_reset_form':UserPasswordResetForm}, name='password_reset'),
    url(r'^accounts/password_reset_confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', 'django.contrib.auth.views.password_reset_confirm', name='password_reset_confirm'),
    url(r'^accounts/password_reset_done/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
    url(r'^accounts/password_reset_complete/$', 'django.contrib.auth.views.password_reset_complete', name='password_reset_complete'),

    url(r'^admin/', include(admin.site.urls)),

)
urlpatterns += patterns('',
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    # url(r'^ckeditor/', include('ckeditor.urls')),
    (r'^ckeditor/', include('ckeditor.urls')),
)
