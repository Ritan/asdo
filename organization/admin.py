# -*- coding: utf-8 -*-
from django.contrib import admin
from models import *
from django.contrib.auth.admin import UserAdmin

class UserProfileInline(admin.StackedInline):
	model = UserProfile
	can_delete = False
	verbose_name_plural = 'Профиль'

class UserAdmin(UserAdmin):
	inlines = [UserProfileInline, ]

admin.site.unregister(User)
admin.site.register(User, UserAdmin)

class UniversityAdmin(admin.ModelAdmin):
	list_display = ('name', 'full_name',)
	search_fields = ('name',)
admin.site.register(University, UniversityAdmin)

class DepartmentAdmin(admin.ModelAdmin):
	list_display = ('name', 'full_name', 'university',)
	list_filter = ('university',)
	search_fields = ('name',)
admin.site.register(Department, DepartmentAdmin)

class SubjectAdmin(admin.ModelAdmin):
	list_display = ('name', 'full_name', 'department',)
	list_filter = ('department',)
	search_fields = ('name',)
admin.site.register(Subject, SubjectAdmin)

class EducationFilesInline(admin.StackedInline):
	model = EducationFiles
	extra = 2

class EducationMaterialAdmin(admin.ModelAdmin):
	list_display = ('name', 'author', 'university', 'department', 'subject', 'pub',)
	list_filter = ('university', 'department', 'subject', 'pub',)
	search_fields = ('name','subject',)
	inlines = [EducationFilesInline,]
admin.site.register(EducationMaterial, EducationMaterialAdmin)

class EducationTestAdmin(admin.ModelAdmin):
	list_display = ('name', 'material', 'author', 'subject', 'department', 'university', 'admin_questions',)
	list_filter = ('material', 'subject', 'department', 'university', 'pub',)
	search_fields = ('name','material',)

admin.site.register(EducationTest, EducationTestAdmin)

class QuestionAnswerInline(admin.TabularInline):
	model = QuestionAnswer
	extra = 1
	max_num = 4

class TestQuestionAdmin(admin.ModelAdmin):
	list_display = ('question', 'test',)
	list_filter = ('test',)
	inlines = [QuestionAnswerInline,]

admin.site.register(TestQuestion, TestQuestionAdmin)


class TestSessionsAdmin(admin.ModelAdmin):
	list_display = ('session_id', 'user', 'test', 'question', 'answer', 'answer_correct', 'timing_correct', 'finish', 'force_finish', 'rating', 'percent',)
	list_filter = ('user', 'test', 'finish', 'force_finish', 'rating',)

admin.site.register(TestSessions, TestSessionsAdmin)