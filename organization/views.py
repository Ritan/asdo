# -*- coding: utf-8 -*-
from __future__ import division
import uuid
import datetime
from django.template.loader import render_to_string
import random
from django.core import serializers
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from models import *
from forms import AddEducationMaterialForm, AddEducationFilesForm, AddEducationTestForm, AddTestQuestionForm
import json
from django.forms.models import inlineformset_factory
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

def init_context(request, context):
    user = request.user
    try:
        user_group = user.groups.all()[0]
    except:
        user_group = 2
    context.update({
        'user_profile': user.profile,
        'user_group':user_group,
        # 'university_list': University.objects.all(),
    })
    return context


@login_required()
def home(request):
    context = {
        'home': True
    }
    context = init_context(request, context)
    return render(request, 'organization/home.html', context)


class Stat(TemplateView):
    def user_is_lector(self, request):
        # определяем не преподаватель ли это
        user_is_lector = False
        if request.user.groups.filter(id__in=[2]): # если преподаватель, то может видеть только свои материалы
            user_is_lector = True
        return user_is_lector

    def get_university_list(self, request, user_is_lector):
        # получаем список доступных университетов (для которых есть тесты)
        university_list = []
        for university in University.objects.all():
            education_test_list = TestSessions.objects.filter(test__university=university, finish=True)
            if user_is_lector:
                education_test_list = education_test_list.filter(test__author=request.user)
            else:
                education_test_list = education_test_list.filter(user=request.user)
            if not university in university_list and education_test_list.count():
                university_list.append(university)
        return university_list

    def get_department_list(self, request, university, user_is_lector):
        # получаем список доступных кафедр (если выбран университет)
        department_list = []
        for department in Department.objects.filter(university=university):
            education_test_list = TestSessions.objects.filter(test__university=university, test__department=department, finish=True)
            if user_is_lector:
                education_test_list = education_test_list.filter(test__author=request.user)
            else:
                education_test_list = education_test_list.filter(user=request.user)
            if not department in department_list and education_test_list.count():
                department_list.append(department)
        return department_list

    def get_subject_list(self, request, university, department, user_is_lector):
        # получаем список доступных дисциплин (если выбран университет и кафедра)
        subject_list = []
        for subject in Subject.objects.filter(department=department):
            education_test_list = TestSessions.objects.filter(test__university=university, test__department=department, test__subject=subject, finish=True)
            if user_is_lector:
                education_test_list = education_test_list.filter(test__author=request.user)
            else:
                education_test_list = education_test_list.filter(user=request.user)
            if not subject in subject_list and education_test_list.count():
                subject_list.append(subject)
        return subject_list

    def get_lector_list(self, request, university, department, subject):
        # получаем список доступных преподавателей (если выбран университет, кафедра и дисциплина)
        lector_list = []
        for lector in User.objects.filter(groups__pk=2):
            education_test_list = TestSessions.objects.filter(test__university=university, test__department=department, test__subject=subject, test__author=lector, finish=True, user=request.user)
            if not lector in lector_list and education_test_list.count():
                lector_list.append(lector)
        return lector_list

class StatListView(Stat):
    template_name = 'organization/stat.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(StatListView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context = init_context(request, context)
        context.update({'stat_page':True})

        user_is_lector = self.user_is_lector(request)

        # if user_is_lector:
        #     edu_test_form = AddEducationTestForm(request.POST or None)
        #     edu_test_form.fields['material'].queryset = EducationMaterial.objects.filter(author=request.user)
        #     context.update({'form': edu_test_form})

        context.update({'university_list':self.get_university_list(request, user_is_lector)})

        # получаем список доступных результатов тестов
        materials_list = TestSessions.objects.filter(finish=True)
        print materials_list

        #Студенты видят только свои результаты
        if not user_is_lector:
            materials_list = materials_list.filter(user=request.user)

        if kwargs.get('university_id'):
            try:
                university = University.objects.get(pk=kwargs.get('university_id'))
                materials_list = materials_list.filter(test__university=university)
                context.update({
                    'university': university,
                    'department_list': self.get_department_list(request, university, user_is_lector)
                })
                if kwargs.get('department_id'):
                    try:
                        department = Department.objects.get(pk=kwargs.get('department_id'))
                        materials_list = materials_list.filter(test__department=department)
                        context.update({
                            'department': department,
                            'subject_list': self.get_subject_list(request, university, department, user_is_lector)
                        })
                        if kwargs.get('subject_id'):
                            try:
                                subject = Subject.objects.get(pk=kwargs.get('subject_id'))
                                print 'subject: ', subject
                                materials_list = materials_list.filter(test__subject=subject)
                                print 'materials_list:', materials_list
                                context.update({'subject': subject})

                                if not user_is_lector: # если студент
                                    context.update({'lector_list': self.get_lector_list(request, university, department, subject)})
                                    if kwargs.get('lector_id'):
                                        print 'lector_id:', kwargs.get('lector_id')
                                        try:
                                            lector = User.objects.get(pk=kwargs.get('lector_id'))
                                            print 'lector', lector
                                            context.update({'lector':lector})
                                        except:
                                            print 'Can not find lector with id: ', kwargs.get('lector_id')
                                            return HttpResponseRedirect(reverse('material_subject', kwargs=({'university_id': university.id, 'department_id':department.id, 'subject_id':subject.id})))
                            except:
                                print 'Can not find subject with id: ', kwargs.get('subject_id')
                                return HttpResponseRedirect(reverse('material_department', kwargs=({'university_id': university.id, 'department_id':department.id})))
                    except:
                        print 'Can not find department with id: ', kwargs.get('department_id')
                        return HttpResponseRedirect(reverse('material_university', kwargs=({'university_id': university.id})))
            except:
                print 'Can not find university with id: ', kwargs.get('university_id')
                return HttpResponseRedirect(reverse('material', kwargs=()))

        print 'materials_list:',materials_list
        context.update({'materials_list': materials_list })

        context.update({'stat':True})
        return self.render_to_response(context)




class Test(TemplateView):
    def user_is_lector(self, request):
        # определяем не преподаватель ли это
        user_is_lector = False
        if request.user.groups.filter(id__in=[2]): # если преподаватель, то может видеть только свои материалы
            user_is_lector = True
        return user_is_lector

    def get_university_list(self, request, user_is_lector):
        # получаем список доступных университетов
        return University.objects.all()

    def get_department_list(self, request, university, user_is_lector):
        # получаем список доступных кафедр (если выбран университет)
        department_list = []
        for department in Department.objects.filter(university=university):
            education_test_list = EducationTest.objects.filter(university=university, department=department)
            if user_is_lector:
                education_test_list = education_test_list.filter(author=request.user)
            else:
                education_test_list = education_test_list.filter(pub=True)
            if not department in department_list:
                department_list.append(department)
        return department_list

    def get_subject_list(self, request, university, department, user_is_lector):
        # получаем список доступных дисциплин (если выбран университет и кафедра)
        subject_list = []
        for subject in Subject.objects.filter(department=department):
            education_test_list = EducationTest.objects.filter(university=university, department=department, subject=subject)
            if user_is_lector:
                education_test_list = education_test_list.filter(author=request.user)
            else:
                education_test_list = education_test_list.filter(pub=True)
            if not subject in subject_list:
                subject_list.append(subject)
        return subject_list

    def get_lector_list(self, request, university, department, subject):
        # получаем список доступных преподавателей (если выбран университет, кафедра и дисциплина)
        lector_list = []
        for lector in User.objects.filter(groups__pk=2):
            education_test_list = EducationTest.objects.filter(university=university, department=department, subject=subject, author=lector, pub=True)
            if not lector in lector_list:
                lector_list.append(lector)
        return lector_list

class TestListView(Test):
    template_name = 'organization/test.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TestListView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context = init_context(request, context)
        context.update({'test_page':True})

        user_is_lector = self.user_is_lector(request)

        if user_is_lector:
            edu_test_form = AddEducationTestForm(request.POST or None)
            edu_test_form.fields['material'].queryset = EducationMaterial.objects.filter(author=request.user)
            context.update({'form': edu_test_form})

        context.update({'university_list':self.get_university_list(request, user_is_lector)})

        # получаем список доступных учебных материалов
        materials_list = EducationTest.objects.all()
        if user_is_lector:
            materials_list = materials_list
        else:
            try:
                publisher = User.objects.get(id=context["lector_id"]).profile
                materials_list = materials_list.filter(pub=publisher)
            except Exception as e:
                print e
                materials_list = []

        if kwargs.get('university_id'):
            try:
                university = University.objects.get(pk=kwargs.get('university_id'))
                #materials_list = materials_list.filter(university=university)
                context.update({
                    'university': university,
                    'department_list': self.get_department_list(request, university, user_is_lector)
                })
                if kwargs.get('department_id'):
                    try:
                        department = Department.objects.get(pk=kwargs.get('department_id'))
                        #materials_list = materials_list.filter(department=department)
                        context.update({
                            'department': department,
                            'subject_list': self.get_subject_list(request, university, department, user_is_lector)
                        })
                        if kwargs.get('subject_id'):
                            try:
                                subject = Subject.objects.get(pk=kwargs.get('subject_id'))
                                print 'subject: ', subject
                                #materials_list = materials_list.filter(subject=subject)
                                print 'materials_list:', materials_list
                                context.update({'subject': subject})

                                if not user_is_lector: # если студент
                                    context.update({'lector_list': self.get_lector_list(request, university, department, subject)})
                                    if kwargs.get('lector_id'):
                                        print 'lector_id:', kwargs.get('lector_id')
                                        try:
                                            lector = User.objects.get(pk=kwargs.get('lector_id'))
                                            print 'lector', lector
                                            context.update({'lector':lector})
                                        except:
                                            print 'Can not find lector with id: ', kwargs.get('lector_id')
                                            return HttpResponseRedirect(reverse('material_subject', kwargs=({'university_id': university.id, 'department_id':department.id, 'subject_id':subject.id})))
                            except:
                                print 'Can not find subject with id: ', kwargs.get('subject_id')
                                return HttpResponseRedirect(reverse('material_department', kwargs=({'university_id': university.id, 'department_id':department.id})))
                    except:
                        print 'Can not find department with id: ', kwargs.get('department_id')
                        return HttpResponseRedirect(reverse('material_university', kwargs=({'university_id': university.id})))
            except:
                print 'Can not find university with id: ', kwargs.get('university_id')
                return HttpResponseRedirect(reverse('material', kwargs=()))

        print 'materials_list:',materials_list
        context.update({'materials_list': materials_list })

        context.update({'test':True})
        return self.render_to_response(context)

class TestView(Test):
    template_name = 'organization/test_view.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TestView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context = init_context(request, context)
        context.update({'test_page':True})

        test = get_object_or_404(EducationTest, pk=kwargs.get('test_id'))

        is_published = test.pub.all().filter(user=request.user).exists()

        user_is_lector = self.user_is_lector(request)

        test_questions = TestQuestion.objects.filter(test=test)
        for i in test_questions:
            i.complexity = COMPLEXITY_CHOICES[i.complexity-1][1]
        context.update({
            'university': test.university,
            'university_list': self.get_university_list(request, user_is_lector),
            'department': test.department,
            'department_list': self.get_department_list(request, test.university, user_is_lector),
            'subject': test.subject,
            'subject_list': self.get_subject_list(request, test.university, test.department, user_is_lector),
            'test':True,
            'page': test,
            'questions': test_questions,
            'is_published' : is_published,
            'user_is_lector' : user_is_lector,
        })

        if user_is_lector:
            if kwargs.get('question_id'):
                question = TestQuestion.objects.get(test=test, pk=kwargs.get('test_id'))
            else:
                question = None

            test_material_id = None
            if test.material:
                test_material_id = test.material.id

            form_initial = {
                'name':test.name,
                'description':test.description,
                'material': test_material_id,
                'level_up': test.level_up,
                'level_down': test.level_down,
                'fail': test.fail,
                'not_fail': test.not_fail,
                'good': test.good,
                # 'excellent': test.excellent
            }
            question_form = AddTestQuestionForm(instance=question, initial={'test':test.pk}, materialid=test_material_id)
            form = AddEducationTestForm(initial=form_initial)
            context.update({'file_form': question_form, 'form':form})

        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        test = get_object_or_404(EducationTest, pk=kwargs.get('test_id'))
        test_material_id = None
        if test.material:
            test_material_id = test.material.id
        form = AddTestQuestionForm(request.POST or None, materialid=test_material_id)
        if test_material_id and form.is_valid():
            question = form.save()
            return HttpResponseRedirect(reverse('question_view', kwargs=({'test_id':test.pk,'question_id': question.pk})))
        else:
            print 'NOT VALID!!!'
        return self.get(request, *args, **kwargs)

@login_required()
def toggle_pub_education_test(request, test_id):
    test = get_object_or_404(EducationTest, pk=test_id)
    url = reverse('test_view', kwargs=({
        'university_id': test.university.id,
        'department_id': test.department.id,
        'subject_id': test.subject.id,
        'lector_id': test.author.id,
        'test_id': test.pk,
    }))
    if request.user.groups.filter(id__in=[2]): # если преподаватель
        max = test.level_up
        if test.level_down > test.level_up:
            max = test.level_down


        if test.get_simple_questions().count() >= max and test.get_normal_questions().count() >= max and test.get_difficult_questions().count() >= max:
            if test.pub.all().filter(user=request.user):
                test.pub.remove(request.user.profile)
            else:
                test.pub.add(request.user.profile)
            test.save()
        else:
            if test.pub.all().filter(user=request.user):
                test.pub.remove(request.user.profile)
                test.save()
    return HttpResponseRedirect(url)

def question_view(request, test_id, question_id):
    test = get_object_or_404(EducationTest, pk=test_id)
    question = get_object_or_404(TestQuestion, pk=question_id, test=test.pk)

    form = AddTestQuestionForm(request.POST or None, instance=question, materialid=test.material.id)
    AnswersFormset = inlineformset_factory(TestQuestion, QuestionAnswer, extra=4, max_num=4, exclude=('',))
    formset = AnswersFormset(request.POST or None, instance=question)
    if request.method == 'POST':

        if form.is_valid():
            question = form.save()
            return HttpResponseRedirect(reverse('question_view', kwargs=({'test_id':test.pk,'question_id': question.pk})))
        else:
            print 'form not valid'
        if formset.is_valid():
            print 'formset valid'
            formset.save()
            return HttpResponseRedirect(reverse('question_view', kwargs=({'test_id':test.pk,'question_id': question.pk})))
        else:
            print 'formset not valid'


    answers = QuestionAnswer.objects.filter(question=question)
    answers_count = answers.count()
    true_answers_count = answers.filter(correct=True).count()


    context = {
        'test': test,
        'question': question,
        'form': form,
        'formset': formset,
        'answers_count':answers_count,
        'true_answers_count': true_answers_count,
    }
    context = init_context(request, context)
    return render(request, 'organization/question_view.html', context)

@login_required()
def toggle_pub_test_question(request, question_id):
    question = get_object_or_404(TestQuestion, pk=question_id)
    url = reverse('question_view', kwargs=({
        'test_id': question.test.id,
        'question_id': question.pk,
    }))
    if request.user.groups.filter(id__in=[2]): # если преподаватель
        answers = QuestionAnswer.objects.filter(question=question)
        answers_count = answers.count()
        true_answers_count = answers.filter(correct=True).count()
        if answers_count > 1 and true_answers_count == 1:
            if question.pub:
                question.pub = False
            else:
                question.pub = True
            question.save()
    return HttpResponseRedirect(url)


@login_required()
def remove_test_question(request, question_id):
    question = get_object_or_404(TestQuestion, pk=question_id)
    test = EducationTest.objects.get(pk=question.test.id)
    print 'question: ', question
    url = reverse('test_view', kwargs=({
        'university_id': test.university.id,
        'department_id':test.department.id,
        'subject_id': test.subject.id,
        'lector_id': test.author.id,
        'test_id': test.pk,
    }))
    if request.user.groups.filter(id__in=[2]): # если преподаватель
        question.delete()
    return HttpResponseRedirect(url)


@csrf_exempt
def add_education_test(request):
    if request.method == 'POST':
        test_id = request.POST.get('test_id')
        if test_id:
            test_id = int(test_id)
        university_id = int(request.POST.get('university'))
        department_id = int(request.POST.get('department'))
        subject_id = int(request.POST.get('subject'))
        material_id = int(request.POST.get('material'))
        name = request.POST.get('name')
        description = request.POST.get('description')
        user = request.user
        university = University.objects.get(pk=university_id)
        department = Department.objects.get(pk=department_id)
        subject = Subject.objects.get(pk=subject_id)
        material = EducationMaterial.objects.get(pk=material_id)
        level_up = int(request.POST.get('level_up'))
        level_down = int(request.POST.get('level_down'))
        fail = request.POST.get('fail')
        not_fail = request.POST.get('not_fail')
        good = request.POST.get('good')
        # excellent  =request.POST.get('excellent')
        if test_id:
            print 'TYT!'
            test = EducationTest.objects.get(pk=test_id)
            test.university = university
            test.department = department
            test.subject = subject
            test.material = material
            test.name = name
            test.description = description
            test.level_up = level_up
            test.level_down = level_down
            test.fail = fail
            test.not_fail = not_fail
            test.good = good
            # test.excellent = excellent
        else:
            test = EducationTest(
                university=university,
                department=department,
                subject=subject,
                material=material,
                name=name,
                description=description,
                author=user,
                level_up=level_up,
                level_down=level_down,
                fail=fail,
                not_fail=not_fail,
                good=good,
                # excellent=excellent
            )
        print 'test.save()'
        test.save()
        print 'test.save()2'
        return HttpResponseRedirect(reverse('test_view', kwargs=({
            'university_id': university.id,
            'department_id':department.id,
            'subject_id':subject.id,
            'lector_id': user.id,
            'test_id':test.pk
         })))
    return HttpResponse(content='')



class Material(TemplateView):

    def user_is_lector(self, request):
        # определяем не преподаватель ли это
        user_is_lector = False
        if request.user.groups.filter(id__in=[2]):
            user_is_lector = True
        return user_is_lector

    def get_university_list(self, request, user_is_lector):
        # получаем список доступных университетов (для которых есть учебные материалы)
        university_list = []
        for university in University.objects.all():
            education_material_list = EducationMaterial.objects.filter(university=university)
            if user_is_lector:
                education_material_list = education_material_list
            else:
                education_material_list = education_material_list.filter(pub=True)
            if not university in university_list and education_material_list.count():
                university_list.append(university)
        return university_list

    def get_department_list(self, request, university, user_is_lector):
        # получаем список доступных кафедр (если выбран университет)
        department_list = []
        for department in Department.objects.filter(university=university):
            education_material_list = EducationMaterial.objects.filter(university=university, department=department)
            if user_is_lector:
                education_material_list = education_material_list
            else:
                education_material_list = education_material_list.filter(pub=True)
            if not department in department_list and education_material_list.count():
                department_list.append(department)
        return department_list

    def get_subject_list(self, request, university, department, user_is_lector):
        # получаем список доступных дисциплин (если выбран университет и кафедра)
        subject_list = []
        for subject in Subject.objects.filter(department=department):
            education_material_list = EducationMaterial.objects.filter(university=university, department=department, subject=subject)
            if user_is_lector:
                education_material_list = education_material_list
            else:
                education_material_list = education_material_list.filter(pub=True)
            if not subject in subject_list and education_material_list.count():
                subject_list.append(subject)
        return subject_list

    def get_lector_list(self, request, university, department, subject):
        # получаем список доступных преподавателей (если выбран университет, кафедра и дисциплина)
        lector_list = []
        for lector in User.objects.filter(groups__pk=2):
            education_material_list = EducationMaterial.objects.filter(university=university, department=department, subject=subject, pub=True)
            if not lector in lector_list and education_material_list.count():
                lector_list.append(lector)
        return lector_list

class MaterialListView(Material):
    template_name = 'organization/material.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MaterialListView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context = init_context(request, context)
        context.update({'materials_page':True})

        user_is_lector = self.user_is_lector(request)

        if user_is_lector:
            context.update({'form': AddEducationMaterialForm(request.POST or None)})

        context.update({'university_list':self.get_university_list(request, user_is_lector)})

        # получаем список доступных учебных материалов
        materials_list = EducationMaterial.objects.all()
        if user_is_lector:
            materials_list = materials_list
        else:
            materials_list = materials_list.filter(pub=True)

        if kwargs.get('university_id'):
            try:
                university = University.objects.get(pk=kwargs.get('university_id'))
                materials_list = materials_list.filter(university=university)
                context.update({
                    'university': university,
                    'department_list': self.get_department_list(request, university, user_is_lector)
                })
                if kwargs.get('department_id'):
                    try:
                        department = Department.objects.get(pk=kwargs.get('department_id'))
                        materials_list = materials_list.filter(department=department)
                        context.update({
                            'department': department,
                            'subject_list': self.get_subject_list(request, university, department, user_is_lector)
                        })
                        if kwargs.get('subject_id'):
                            try:
                                subject = Subject.objects.get(pk=kwargs.get('subject_id'))
                                print 'subject: ', subject
                                materials_list = materials_list.filter(subject=subject)
                                print 'materials_list:', materials_list
                                context.update({'subject': subject})

                                if not user_is_lector: # если студент
                                    context.update({'lector_list': self.get_lector_list(request, university, department, subject)})
                                    if kwargs.get('lector_id'):
                                        print 'lector_id: ', kwargs.get('lector_id'), type(kwargs.get('lector_id'))
                                        try:
                                            lector = User.objects.get(pk=int(kwargs.get('lector_id')))
                                            print 'lector: ', lector, lector.get_full_name()
                                            materials_list = materials_list.filter(author=lector)
                                            context.update({'lector':lector})
                                        except:
                                            print 'Can not find lector with id: ', kwargs.get('lector_id')
                                            return HttpResponseRedirect(reverse('material_subject', kwargs=({'university_id': university.id, 'department_id':department.id, 'subject_id':subject.id})))
                            except:
                                print 'Can not find subject with id: ', kwargs.get('subject_id')
                                return HttpResponseRedirect(reverse('material_department', kwargs=({'university_id': university.id, 'department_id':department.id})))
                    except:
                        print 'Can not find department with id: ', kwargs.get('department_id')
                        return HttpResponseRedirect(reverse('material_university', kwargs=({'university_id': university.id})))
            except:
                print 'Can not find university with id: ', kwargs.get('university_id')
                return HttpResponseRedirect(reverse('material', kwargs=()))

        print 'materials_list:',materials_list
        context.update({'materials_list': materials_list })

        context.update({'material':True})
        return self.render_to_response(context)




class MaterialView(Material):
    template_name = 'organization/material_view.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MaterialView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context = init_context(request, context)
        context.update({'materials_page':True})

        material = get_object_or_404(EducationMaterial, pk=kwargs.get('material_id'))
        user_is_lector = self.user_is_lector(request)
        context.update({
            'university': material.university,
            'university_list': self.get_university_list(request, user_is_lector),
            'department': material.department,
            'department_list': self.get_department_list(request, material.university, user_is_lector),
            'subject': material.subject,
            'subject_list': self.get_subject_list(request, material.university, material.department, user_is_lector),
            'lector': material.author,
            'material':True,
            'page': material,
            'files': EducationFiles.objects.filter(material=material),
        })

        if user_is_lector:
            if kwargs.get('file_id'):
                education_file = EducationFiles.objects.get(material=material, pk=kwargs.get('file_id'))
            else:
                education_file = None

            if request.method == 'POST':
                file_form = AddEducationFilesForm(request.POST, request.FILES, instance=education_file)
                form = AddEducationMaterialForm(request.POST or None, initial={'name':material.name, 'description':material.description})
            else:
                file_form = AddEducationFilesForm(instance=education_file)
                form = AddEducationMaterialForm(initial={'name':material.name, 'description':material.description})
            context.update({'file_form': file_form, 'form':form})

        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        print 'post'
        material = get_object_or_404(EducationMaterial, pk=kwargs.get('material_id'))
        if kwargs.get('file_id'):
            education_file = EducationFiles(material=material, pk=kwargs.get('file_id'))
        else:
            education_file = None
        if request.FILES:
            form = AddEducationFilesForm(request.POST, request.FILES or None, instance=education_file)
        else:
            form = AddEducationFilesForm(request.POST or None, instance=education_file)
        if form.is_valid():
            education_file = form.save(commit=False)
            education_file.material = material
            education_file.save()
            print 'saved'
            return HttpResponseRedirect(reverse('material_view', kwargs=({'university_id': material.university.id, 'department_id':material.department.id, 'subject_id':material.subject.id, 'lector_id': material.author.id, 'material_id':material.pk})))
        return self.get(request, *args, **kwargs)



@csrf_exempt
def add_education_material(request):
    if request.method == 'POST':
        material_id = request.POST.get('material_id')
        if material_id:
            material_id = int(material_id)
        university_id = int(request.POST.get('university'))
        department_id = int(request.POST.get('department'))
        subject_id = int(request.POST.get('subject'))
        name = request.POST.get('name')
        description = request.POST.get('description')
        user = request.user
        university = University.objects.get(pk=university_id)
        department = Department.objects.get(pk=department_id)
        subject = Subject.objects.get(pk=subject_id)
        if material_id:
            education_material = EducationMaterial.objects.get(pk=material_id)
            education_material.university = university
            education_material.department = department
            education_material.subject = subject
            education_material.name = name
            education_material.description = description
        else:
            education_material = EducationMaterial(
                university=university,
                department=department,
                subject=subject,
                name=name,
                description=description,
                author=user,
                pub=False
            )
        education_material.save()
        return HttpResponseRedirect(reverse('material_view', kwargs=({
            'university_id': university.id,
            'department_id':department.id,
            'subject_id':subject.id,
            'lector_id': user.id,
            'material_id':education_material.pk
         })))
    return HttpResponse(content='')

@login_required()
def remove_education_material_file(request, file_id):
    edu_file = get_object_or_404(EducationFiles, pk=file_id)
    url = reverse('material_view', kwargs=({
        'university_id': edu_file.material.university.id,
        'department_id':edu_file.material.department.id,
        'subject_id':edu_file.material.subject.id,
        'lector_id': edu_file.material.author.id,
        'material_id':edu_file.material.pk
    }))
    if request.user.groups.filter(id__in=[2]): # если преподаватель
        edu_file.delete()
    return HttpResponseRedirect(url)

@login_required()
def remove_education_material(request, material_id):
    material = get_object_or_404(EducationMaterial, pk=material_id)
    print 'material: ', material
    url = reverse('material_subject', kwargs=({
        'university_id': material.university.id,
        'department_id':material.department.id,
        'subject_id': material.subject.id,
    }))
    if request.user.groups.filter(id__in=[2]): # если преподаватель
        material.delete()
    return HttpResponseRedirect(url)

@login_required()
def toggle_pub_education_material(request, material_id):
    material = get_object_or_404(EducationMaterial, pk=material_id)
    url = reverse('material_view', kwargs=({
        'university_id': material.university.id,
        'department_id':material.department.id,
        'subject_id': material.subject.id,
        'lector_id': material.author.id,
        'material_id': material.pk,
    }))
    if request.user.groups.filter(id__in=[2]): # если преподаватель
        if material.pub:
            material.pub = False
        else:
            material.pub = True
        material.save()
    return HttpResponseRedirect(url)







def get_university_list(request):
    university_list = [{'name':item.name, 'full_name':item.full_name, 'id':item.pk} for item in University.objects.all()]
    return HttpResponse(content=json.dumps(university_list), content_type='application/json')

def get_department_list(request):
    university_id = request.GET.get('university_id') or request.GET.get('university')
    if university_id:
        university_id = int(university_id)
        department_list = [{'name':item.name, 'full_name':item.full_name, 'id':item.pk} for item in Department.objects.filter(university=university_id)]
        return HttpResponse(content=json.dumps(department_list), content_type='application/json')
    return HttpResponse(content='')

def get_subject_list(request):
    department_id = request.GET.get('department_id') or request.GET.get('department')
    if department_id:
        department_id = int(department_id)
        subject_list = [{'name':item.name, 'full_name':item.full_name, 'id':item.pk} for item in Subject.objects.filter(department=department_id)]
        return HttpResponse(content=json.dumps(subject_list), content_type='application/json')
    return HttpResponse(content='')


def get_lector_list(request):
    subject_id = request.GET.get('subject_id')
    print 'subject_id: ',subject_id
    if subject_id:
        subject_id = int(subject_id)
        lector_list = [{'name':item.profile.get_name(), 'full_name':item.profile.get_full_name(), 'id':item.pk} for item in User.objects.filter()]
        return HttpResponse(content=json.dumps(lector_list), content_type='application/json')
    return HttpResponse(content='')









def gen_uuid():
    return str(uuid.uuid4())

def get_new_uuid(request):
    return HttpResponse(content=gen_uuid())

def init_test(request):
    test_id = request.GET.get('test_id')
    session = request.COOKIES.get('user_test_session')
    print request.COOKIES
    if test_id:
        test_id = int(test_id)
        test = EducationTest.objects.get(pk=test_id)
        test_data = {
            'name': test.name,
        }
        response = HttpResponse(content=json.dumps(test_data), content_type='application/json')
        if not session:
            response.set_cookie('user_test_session', gen_uuid(), path='/')
        return response

def get_next_question(request):
    test_id = request.GET.get('test_id')
    session = request.COOKIES.get('user_test_session')
    if test_id and session:
        test_id = int(test_id)
        test = EducationTest.objects.get(pk=test_id)
        user = request.user




        # получаем все вопросы в тесте
        available_questions = TestQuestion.objects.filter(test=test, pub=True)
        # получаем вопросы которые были заданы
        exist_questions = TestSessions.objects.filter(test=test, session_id=session, user=user).order_by('question_start')


        if exist_questions.count() == 0:    # значит это первый вопрос
            print 'первый вопрос в тесте - значит берем простой'
            # первый вопрос в тесте всегда простой
            available_questions = available_questions.filter(complexity=1)
        else:
            print 'следующий вопрос в тесте, исключаем те, что уже были заданы'
            print '-------------------------------'
            # исключаем вопросы которые уже задавались (то есть есть в таблице сессии теста)
            available_questions = available_questions.exclude(pk__in=[s.question.pk for s in exist_questions])


            # определяем сколько переходов по уровням было совершено
            max_level_down_count = 2        # максимальное количество понижений уровня сложности
            level_up_count = 0              # количество повышений уровня сложности
            level_down_count = 0            # количество понижений уровня сложности
            answer_correct_count = 0        # количество правильных ответов
            answer_not_correct_count = 0    # количество неправильный ответов

            last_question = exist_questions.order_by('-question_finish')[0]
            # определяем текущую сложность вопросов
            # current_complexity = last_question.question.complexity
            # new_complexity = current_complexity
            new_complexity = 1

            print '\n\tНачинаем цикл по определению уровня сложности\n'
            print 'Задано вопросов: ', exist_questions.count()

            for q in exist_questions:
                if q.answer_correct:
                    answer_correct_count += 1
                    if answer_correct_count == test.level_up:  # если количство вопросов этого уровня удовлетворяют повышению сложности
                        print 'LEVEL UP '
                        level_up_count += 1
                        print 'answer_correct_count: ', answer_correct_count, 'answer_not_correct_count: ',answer_not_correct_count, ' ОБНУЛЯЕМ'
                        answer_correct_count = 0
                        answer_not_correct_count = 0
                        if q.question.complexity < 3:
                            print 'повышаем уровень сложности'
                            new_complexity = q.question.complexity+1
                            print 'new_complexity: ', new_complexity
                        if q.question.complexity == 3:   # если достигнут максимальный уровень сложности
                            # отлично, тест прошел
                            print 'MAX COMPLEXITY - FINISH'
                            return finish_test(request, test, session, user)
                    else:
                        print 'оставляем текущий уровень сложности'
                else:
                    answer_not_correct_count += 1
                    if answer_not_correct_count == test.level_down:
                        print 'LEVEL DOWN'
                        level_down_count += 1
                        print 'answer_correct_count: ', answer_correct_count, 'answer_not_correct_count: ',answer_not_correct_count, ' ОБНУЛЯЕМ'
                        answer_not_correct_count = 0
                        answer_correct_count = 0
                        if level_down_count == max_level_down_count:
                            print 'провал достигнуто максимальнео количество понимажений уровня'
                            return finish_test(request, test, session, user)
                        if q.question.complexity > 1:
                            print 'понижаем уровень сложности'
                            new_complexity = q.question.complexity-1
                            print 'new_complexity: ', new_complexity
                        else:
                            print 'q.question.complexity <= 1 - FINISH'
                            return finish_test(request, test, session, user)
                    else:
                        print 'оставляем текущий уровень сложности'
                    print 'Кол-во правильных ответов: ', answer_correct_count
                    print 'Кол-во неправильных ответов: ', answer_not_correct_count

            available_questions = available_questions.filter(complexity=new_complexity)



        if not available_questions.count():
            response = HttpResponse(content='No questions')
            response.set_cookie('user_test_session','')
            print 'Нет больше вопросов требуемого уровня - завершаем тест'
            return finish_test(request, test, session, user)



        question_index = random.choice([i for i in range(available_questions.count())])
        question = available_questions[question_index]

        answers = []
        for a in QuestionAnswer.objects.filter(question=question):
            answers.append({'answer': a.answer, 'id': a.id, 'correct': a.correct})

        test_session = TestSessions(session_id = session, user = request.user, test = test, question = question)
        test_session.save()
        response = render(request, 'organization/inc/answers_form.html', {'question': question, 'answers': answers})
        return response


def finish_test(request, test, session, user):
    print 'finish_test'
    # получаем вопросы которые были заданы
    exist_questions = TestSessions.objects.filter(test=test, session_id=session, user=user)
    last_question = exist_questions.order_by('-question_finish')[0]
    last_question.finish = True
    last_question.save()

    not_correct_answers = exist_questions.filter(answer_correct=False)
    materials = []
    for i in not_correct_answers:
        if i.question.material_part not in materials:
            materials.append(i.question.material_part)

    # получаем все вопросы в тесте
    available_questions = TestQuestion.objects.filter(test=test, pub=True)

    # вычисляем оценку - rating
    rating = 0

    # 1. Рассчитаем количество баллов за правильный ответ на 1 вопрос простого уровня сложности в выбранном тесте
    # всего вопросов/из них простых*заранее рассчитанный коэфф.
    Mu_a = available_questions.count()/available_questions.filter(complexity=1).count()*1
    print '%s/%s' % (available_questions.count(), available_questions.filter(complexity=1).count())
    print 'Mu_a: ', Mu_a
    # 2. Рассчитаем количество баллов за правильный ответ на 1 вопрос среднего уровня сложности в выбранном тесте
    # всего вопросов/из них средних*заранее рассчитанный коэфф.
    Mu_b = available_questions.count()/available_questions.filter(complexity=2).count()*2.5
    print '%s/%s *2.5' % (available_questions.count(), available_questions.filter(complexity=2).count())
    print 'Mu_b: ', Mu_b
    # 3. Рассчитаем количество баллов за правильный ответ на 1 вопрос сложного уровня сложности в выбранном тесте:
    # всего вопросов/из них средних*заранее рассчитанный коэфф.
    Mu_c = available_questions.count()/available_questions.filter(complexity=3).count()*4.625
    print '%s/%s *4.625' % (available_questions.count(), available_questions.filter(complexity=3).count())
    print 'Mu_c: ', Mu_c
    # 4. Рассчитаем максимально возможное количество баллов за ответы на простые вопросы в пройденном тесте
    V_a = 0
    for i in exist_questions.filter(question__complexity=1):
        timing_coef = 1
        # (МЮ*количество вопросов в пройденном тесте сложного уровня сложности* коэф. Успеваемости во времени ответа на вопрос(равен=1))
        V_a += Mu_a*timing_coef
    print 'V_a: ', V_a
    # 5. Рассчитаем максимально возможное количество баллов за ответы на средние вопросы в пройденном тесте
    V_b = 0
    for i in exist_questions.filter(question__complexity=2):
        timing_coef = 1
        # (МЮ*количество вопросов в пройденном тесте сложного уровня сложности* коэф. Успеваемости во времени ответа на вопрос(равен=1))
        V_b += Mu_b*timing_coef
    print 'V_b: ', V_b
    # 6. Рассчитаем максимально возможное количество баллов за ответы на сложные вопросы в пройденном тесте
    V_c = 0
    for i in exist_questions.filter(question__complexity=3):
        timing_coef = 1
        # (МЮ*количество вопросов в пройденном тесте сложного уровня сложности* коэф. Успеваемости во времени ответа на вопрос(равен=1))
        V_c += Mu_c*timing_coef
    print 'V_c: ', V_c
    # 7. Рассчитаем максимально возможное количество баллов за пройденный тест
    # (Va+Vb+Vc)
    V = V_a + V_b + V_c
    print 'V: ', V
    # 8. Рассчитаем количество баллов за правильные ответы на вопросы простого уровня сложности в пройденном тесте
    V_ap = 0
    for i in exist_questions.filter(question__complexity=1, answer_correct=True):
        timing_coef = 1
        if not i.timing_correct:
            timing_coef = 0.8
        # (МЮ*количество вопросов в пройденном тесте сложного уровня сложности* коэф. Успеваемости во времени ответа на вопрос(равен=1))
        V_ap += Mu_a*timing_coef
    print 'V_ap: ', V_ap
    # 9. Рассчитаем количество баллов за правильные ответы на вопросы среднего уровня сложности в пройденном тесте
    V_bp = 0
    for i in exist_questions.filter(question__complexity=2, answer_correct=True):
        timing_coef = 1
        if not i.timing_correct:
            timing_coef = 0.8
        # (МЮ*количество вопросов в пройденном тесте сложного уровня сложности* коэф. Успеваемости во времени ответа на вопрос(равен=1))
        V_bp += Mu_b*timing_coef
    print 'V_bp: ', V_bp
    # 10. Рассчитаем количество баллов за правильные ответы на вопросы сложного уровня сложности в пройденном тесте
    V_cp = 0
    for i in exist_questions.filter(question__complexity=3, answer_correct=True):
        timing_coef = 1
        if not i.timing_correct:
            timing_coef = 0.8
        # (МЮ*количество вопросов в пройденном тесте сложного уровня сложности* коэф. Успеваемости во времени ответа на вопрос(равен=1))
        V_cp += Mu_c*timing_coef
    print 'V_cp: ', V_cp
    # 11. Рассчитаем количество набранных баллов за пройденный тест
    # Vap+Vbp+Vcp
    Vp = V_ap + V_bp + V_cp
    print 'Vp: ', Vp
    # 12. Рассчитаем процент набранный студентом за прохождение теста
    # Oc = Vp/V*100%
    Oc = Vp/V*100
    print 'Oc: ', Oc, round(Oc, 2)
    # 13. Сравниваем с заданной градацией оценки
    Oc = round(Oc, 2)
    if Oc < test.fail:
        rating = 2
    elif Oc >= test.fail and Oc < test.not_fail:
        rating = 3
    elif Oc >= test.not_fail and Oc < test.good:
        rating = 4
    elif Oc >= test.good:
        rating = 5

    last_question.rating = rating
    last_question.percent = Oc
    last_question.save()



    context = {
        'exist_questions': exist_questions,
        'not_correct_answers': not_correct_answers,
        'material_parts': materials,
        'rating': rating,
        'procent': Oc,
    }
    if materials:
        context.update({
            'material': materials[0].material
        })
    response = render(request, 'organization/inc/test_finish.html', context)
    response.set_cookie('user_test_session','', path='/')
    return response


# аварийное завершение теста, например закрытие окна тестирования
def force_finish_test(request):
    test_id = request.GET.get('test_id')
    session = request.COOKIES.get('user_test_session')
    if test_id and session:
        test_id = int(test_id)
        test = EducationTest.objects.get(pk=test_id)
        user = request.user
        exist_questions = TestSessions.objects.filter(test=test, session_id=session, user=user)
        last_question = exist_questions.order_by('-question_finish')[0]
        last_question.force_finish = True
        last_question.save()
        response = HttpResponse(content='Аварийное завершение теста')
        response.set_cookie('user_test_session','', path='/')
    response = HttpResponse(content='Аварийное завершение теста не удалось')
    return response




def save_question(request):
    test_id = request.GET.get('test_id')
    question_id = request.GET.get('question_id')
    answer_id = request.GET.get('answer_id')
    if test_id and question_id and answer_id:
        test_id = int(test_id)
        question_id = int(question_id)
        answer_id = int(answer_id)
        test = EducationTest.objects.get(pk=test_id)
        question = TestQuestion.objects.get(pk=question_id)
        answer = QuestionAnswer.objects.get(pk=answer_id)
        print 'test: ', test
        print 'question: ', question
        print 'answer: ', answer
        cookies_session = request.COOKIES.get('user_test_session')
        print 'cookies_session: ', cookies_session

        question_finish = datetime.datetime.now().replace(tzinfo=None)



        sesion = TestSessions.objects.get(session_id=cookies_session, test=test.pk, question=question.pk, user=request.user.pk)


        sesion.answer = answer
        sesion.answer_correct = answer.correct

        sesion.question_finish = question_finish

        # question.time
        timing_delta = question_finish - sesion.question_start.replace(tzinfo=None)
        seconds = timing_delta.total_seconds()
        minutes = (seconds % 3600) / 60
        print 'timing_delta: ', timing_delta, ' ', minutes, type(minutes)
        if question.time < minutes:
            sesion.timing_correct = False
        else:
            sesion.timing_correct = True

        sesion.save()
        return HttpResponse(content='saved')
