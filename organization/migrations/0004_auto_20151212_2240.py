# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0003_auto_20151208_1946'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='testsessions',
            options={'ordering': ['-question_finish'], 'verbose_name': '\u0421\u0435\u0441\u0441\u0438\u044f \u0442\u0435\u0441\u0442\u0438\u0440\u043e\u0432\u0430\u044f', 'verbose_name_plural': '\u0421\u0435\u0441\u0441\u0438\u0438 \u0442\u0435\u0441\u0442\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u044f'},
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='status',
            field=models.ForeignKey(verbose_name=b'\xd0\xa1\xd1\x82\xd0\xb0\xd1\x82\xd1\x83\xd1\x81 \xd0\xbf\xd0\xbe\xd0\xbb\xd1\x8c\xd0\xb7\xd0\xbe\xd0\xb2\xd0\xb0\xd1\x82\xd0\xb5\xd0\xbb\xd1\x8f', to='auth.Group', null=True),
        ),
    ]
