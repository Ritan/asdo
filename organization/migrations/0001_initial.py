# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 \xd0\xba\xd0\xb0\xd1\x84\xd0\xb5\xd0\xb4\xd1\x80\xd1\x8b')),
                ('full_name', models.CharField(max_length=255, verbose_name=b'\xd0\x9f\xd0\xbe\xd0\xbb\xd0\xbd\xd0\xbe\xd0\xb5 \xd0\xbd\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 \xd0\xba\xd0\xb0\xd1\x84\xd0\xb5\xd0\xb4\xd1\x80\xd1\x8b')),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0444\u0435\u0434\u0440\u0430',
                'verbose_name_plural': '\u041a\u0430\u0444\u0435\u0434\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='EducationFiles',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd0\xa0\xd0\xb0\xd0\xb7\xd0\xb4\xd0\xb5\xd0\xbb \xd0\xba\xd1\x83\xd1\x80\xd1\x81\xd0\xb0')),
                ('file', models.FileField(upload_to=b'materials', verbose_name=b'\xd0\xa4\xd0\xb0\xd0\xb9\xd0\xbb')),
                ('upload_dt', models.DateField(auto_now_add=True, verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd0\xb7\xd0\xb0\xd0\xb3\xd1\x80\xd1\x83\xd0\xb7\xd0\xba\xd0\xb8')),
            ],
        ),
        migrations.CreateModel(
            name='EducationMaterial',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 \xd0\xbc\xd0\xb0\xd1\x82\xd0\xb5\xd1\x80\xd0\xb8\xd0\xb0\xd0\xbb\xd0\xb0')),
                ('description', models.TextField(verbose_name=b'\xd0\x9e\xd0\xbf\xd0\xb8\xd1\x81\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5', blank=True)),
                ('last_update_dt', models.DateTimeField(auto_now=True, verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd0\xbf\xd0\xbe\xd1\x81\xd0\xbb\xd0\xb5\xd0\xb4\xd0\xbd\xd0\xb5\xd0\xb3\xd0\xbe \xd0\xb8\xd0\xb7\xd0\xbc\xd0\xb5\xd0\xbd\xd0\xb5\xd0\xbd\xd0\xb8\xd1\x8f')),
                ('create_dt', models.DateTimeField(auto_now_add=True, verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd1\x81\xd0\xbe\xd0\xb7\xd0\xb4\xd0\xb0\xd0\xbd\xd0\xb8\xd1\x8f')),
                ('pub', models.BooleanField(default=True, verbose_name=b'\xd0\x9e\xd0\xbf\xd1\x83\xd0\xb1\xd0\xbb\xd0\xb8\xd0\xba\xd0\xbe\xd0\xb2\xd0\xb0\xd1\x82\xd1\x8c')),
                ('author', models.ForeignKey(verbose_name=b'\xd0\x90\xd0\xb2\xd1\x82\xd0\xbe\xd1\x80 (\xd0\xbf\xd1\x80\xd0\xb5\xd0\xbf\xd0\xbe\xd0\xb4\xd0\xb0\xd0\xb2\xd0\xb0\xd1\x82\xd0\xb5\xd0\xbb\xd1\x8c)', to=settings.AUTH_USER_MODEL)),
                ('department', models.ForeignKey(verbose_name=b'\xd0\x9a\xd0\xb0\xd1\x84\xd0\xb5\xd0\xb4\xd1\x80\xd0\xb0', to='organization.Department')),
            ],
            options={
                'verbose_name': '\u0423\u0447\u0435\u0431\u043d\u044b\u0439 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b',
                'verbose_name_plural': '\u0423\u0447\u0435\u0431\u043d\u044b\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u044b',
            },
        ),
        migrations.CreateModel(
            name='EducationTest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 \xd1\x82\xd0\xb5\xd1\x81\xd1\x82\xd0\xb0')),
                ('description', models.TextField(verbose_name=b'\xd0\x9e\xd0\xbf\xd0\xb8\xd1\x81\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5', blank=True)),
                ('last_update_dt', models.DateTimeField(auto_now=True, verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd0\xbf\xd0\xbe\xd1\x81\xd0\xbb\xd0\xb5\xd0\xb4\xd0\xbd\xd0\xb5\xd0\xb3\xd0\xbe \xd0\xb8\xd0\xb7\xd0\xbc\xd0\xb5\xd0\xbd\xd0\xb5\xd0\xbd\xd0\xb8\xd1\x8f')),
                ('create_dt', models.DateTimeField(auto_now_add=True, verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd1\x81\xd0\xbe\xd0\xb7\xd0\xb4\xd0\xb0\xd0\xbd\xd0\xb8\xd1\x8f')),
                ('pub', models.BooleanField(default=True, verbose_name=b'\xd0\x9e\xd0\xbf\xd1\x83\xd0\xb1\xd0\xbb\xd0\xb8\xd0\xba\xd0\xbe\xd0\xb2\xd0\xb0\xd1\x82\xd1\x8c')),
                ('level_up', models.IntegerField(default=0, verbose_name=b'\xd0\x9a\xd0\xbe\xd0\xbb\xd0\xb8\xd1\x87\xd0\xb5\xd1\x81\xd1\x82\xd0\xb2\xd0\xbe \xd0\xbf\xd1\x80\xd0\xb0\xd0\xb2\xd0\xb8\xd0\xbb\xd1\x8c\xd0\xbd\xd1\x8b\xd0\xb9\xd1\x85 \xd0\xbe\xd1\x82\xd0\xb2\xd0\xb5\xd1\x82\xd0\xbe\xd0\xb2 \xd0\xb4\xd0\xbb\xd1\x8f \xd0\xbf\xd0\xbe\xd0\xb2\xd1\x8b\xd1\x88\xd0\xb5\xd0\xbd\xd0\xb8\xd1\x8f \xd1\x83\xd1\x80\xd0\xbe\xd0\xb2\xd0\xbd\xd1\x8f \xd1\x81\xd0\xbb\xd0\xbe\xd0\xb6\xd0\xbd\xd0\xbe\xd1\x81\xd1\x82\xd0\xb8 \xd0\xb2\xd0\xbe\xd0\xbf\xd1\x80\xd0\xbe\xd1\x81\xd0\xb0')),
                ('level_down', models.IntegerField(default=0, verbose_name=b'\xd0\x9a\xd0\xbe\xd0\xbb\xd0\xb8\xd1\x87\xd0\xb5\xd1\x81\xd1\x82\xd0\xb2\xd0\xbe \xd0\xbd\xd0\xb5\xd0\xbf\xd1\x80\xd0\xb0\xd0\xb2\xd0\xb8\xd0\xbb\xd1\x8c\xd0\xbd\xd1\x8b\xd1\x85 \xd0\xbe\xd1\x82\xd0\xb2\xd0\xb5\xd1\x82\xd0\xbe\xd0\xb2 \xd0\xb4\xd0\xbb\xd1\x8f \xd0\xbf\xd0\xbe\xd0\xbd\xd0\xb8\xd0\xb6\xd0\xb5\xd0\xbd\xd0\xb8\xd1\x8f \xd1\x83\xd1\x80\xd0\xbe\xd0\xb2\xd0\xbd\xd1\x8f \xd1\x81\xd0\xbb\xd0\xbe\xd0\xb6\xd0\xbd\xd0\xbe\xd1\x81\xd1\x82\xd0\xb8 \xd0\xb2\xd0\xbe\xd0\xbf\xd1\x80\xd0\xbe\xd1\x81\xd0\xb0')),
                ('fail', models.DecimalField(help_text=b'\xd0\x9e\xd1\x86\xd0\xb5\xd0\xbd\xd0\xba\xd0\xb0 2', verbose_name=b'\xd0\x9c\xd0\xb5\xd0\xbd\xd1\x8c\xd1\x88\xd0\xb5', max_digits=5, decimal_places=2)),
                ('not_fail', models.DecimalField(help_text=b'\xd0\x9e\xd1\x86\xd0\xb5\xd0\xbd\xd0\xba\xd0\xb0 3', verbose_name=b'\xd0\x9c\xd0\xb5\xd0\xbd\xd1\x8c\xd1\x88\xd0\xb5', max_digits=5, decimal_places=2)),
                ('good', models.DecimalField(help_text=b'\xd0\x9e\xd1\x86\xd0\xb5\xd0\xbd\xd0\xba\xd0\xb0 4', verbose_name=b'\xd0\x9c\xd0\xb5\xd0\xbd\xd1\x8c\xd1\x88\xd0\xb5', max_digits=5, decimal_places=2)),
                ('author', models.ForeignKey(verbose_name=b'\xd0\x90\xd0\xb2\xd1\x82\xd0\xbe\xd1\x80 (\xd0\xbf\xd1\x80\xd0\xb5\xd0\xbf\xd0\xbe\xd0\xb4\xd0\xb0\xd0\xb2\xd0\xb0\xd1\x82\xd0\xb5\xd0\xbb\xd1\x8c)', to=settings.AUTH_USER_MODEL)),
                ('department', models.ForeignKey(verbose_name=b'\xd0\x9a\xd0\xb0\xd1\x84\xd0\xb5\xd0\xb4\xd1\x80\xd0\xb0', to='organization.Department')),
                ('material', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name=b'\xd0\xa3\xd1\x87\xd0\xb5\xd0\xb1\xd0\xbd\xd1\x8b\xd0\xb9 \xd0\xbc\xd0\xb0\xd1\x82\xd0\xb5\xd1\x80\xd0\xb8\xd0\xb0\xd0\xbb', blank=True, to='organization.EducationMaterial', null=True)),
            ],
            options={
                'verbose_name': '\u0422\u0435\u0441\u0442',
                'verbose_name_plural': '\u0422\u0435\u0441\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='QuestionAnswer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer', models.CharField(max_length=255, verbose_name=b'\xd0\x92\xd0\xb0\xd1\x80\xd0\xb8\xd0\xb0\xd0\xbd\xd1\x82 \xd0\xbe\xd1\x82\xd0\xb2\xd0\xb5\xd1\x82\xd0\xb0')),
                ('correct', models.BooleanField(default=False, verbose_name=b'\xd0\x9f\xd1\x80\xd0\xb0\xd0\xb2\xd0\xb8\xd0\xbb\xd1\x8c\xd0\xbd\xd1\x8b\xd0\xb9 \xd0\xbe\xd1\x82\xd0\xb2\xd0\xb5\xd1\x82')),
            ],
            options={
                'verbose_name': '\u041e\u0442\u0432\u0435\u0442',
                'verbose_name_plural': '\u041e\u0442\u0432\u0435\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='Subject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 \xd0\xb4\xd0\xb8\xd1\x81\xd1\x86\xd0\xb8\xd0\xbf\xd0\xbb\xd0\xb8\xd0\xbd\xd1\x8b')),
                ('full_name', models.CharField(max_length=255, verbose_name=b'\xd0\x9f\xd0\xbe\xd0\xbb\xd0\xbd\xd0\xbe\xd0\xb5 \xd0\xbd\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 \xd0\xb4\xd0\xb8\xd1\x81\xd1\x86\xd0\xb8\xd0\xbf\xd0\xbb\xd0\xb8\xd0\xbd\xd1\x8b')),
                ('department', models.ForeignKey(verbose_name=b'\xd0\x9a\xd0\xb0\xd1\x84\xd0\xb5\xd0\xb4\xd1\x80\xd0\xb0', to='organization.Department')),
            ],
            options={
                'verbose_name': '\u0414\u0438\u0441\u0446\u0438\u043f\u043b\u0438\u043d\u0430',
                'verbose_name_plural': '\u0414\u0438\u0441\u0446\u0438\u043f\u043b\u0438\u043d\u044b',
            },
        ),
        migrations.CreateModel(
            name='TestQuestion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.IntegerField(default=0, verbose_name=b'\xd0\x9d\xd0\xbe\xd0\xbc\xd0\xb5\xd1\x80 \xd0\xb2\xd0\xbe\xd0\xbf\xd1\x80\xd0\xbe\xd1\x81\xd0\xb0')),
                ('question', ckeditor.fields.RichTextField(verbose_name=b'\xd0\x92\xd0\xbe\xd0\xbf\xd1\x80\xd0\xbe\xd1\x81')),
                ('description', models.TextField(verbose_name=b'\xd0\x9e\xd0\xbf\xd0\xb8\xd1\x81\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5', blank=True)),
                ('last_update_dt', models.DateTimeField(auto_now=True, verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd0\xbf\xd0\xbe\xd1\x81\xd0\xbb\xd0\xb5\xd0\xb4\xd0\xbd\xd0\xb5\xd0\xb3\xd0\xbe \xd0\xb8\xd0\xb7\xd0\xbc\xd0\xb5\xd0\xbd\xd0\xb5\xd0\xbd\xd0\xb8\xd1\x8f')),
                ('create_dt', models.DateTimeField(auto_now_add=True, verbose_name=b'\xd0\x94\xd0\xb0\xd1\x82\xd0\xb0 \xd1\x81\xd0\xbe\xd0\xb7\xd0\xb4\xd0\xb0\xd0\xbd\xd0\xb8\xd1\x8f')),
                ('pub', models.BooleanField(default=False, verbose_name=b'\xd0\x9e\xd0\xbf\xd1\x83\xd0\xb1\xd0\xbb\xd0\xb8\xd0\xba\xd0\xbe\xd0\xb2\xd0\xb0\xd1\x82\xd1\x8c')),
                ('time', models.IntegerField(default=1, verbose_name=b'\xd0\x92\xd1\x80\xd0\xb5\xd0\xbc\xd1\x8f \xd0\xbe\xd1\x82\xd0\xb2\xd0\xb5\xd1\x82\xd0\xb0 \xd0\xbd\xd0\xb0 \xd0\xb2\xd0\xbe\xd0\xbf\xd1\x80\xd0\xbe\xd1\x81 (\xd0\xbc\xd0\xb8\xd0\xbd)')),
                ('complexity', models.IntegerField(default=1, verbose_name=b'\xd0\xa1\xd0\xbb\xd0\xbe\xd0\xb6\xd0\xbd\xd0\xbe\xd1\x81\xd1\x82\xd1\x8c \xd0\xb2\xd0\xbe\xd0\xbf\xd1\x80\xd0\xbe\xd1\x81\xd0\xb0', choices=[(1, b'\xd0\x9f\xd1\x80\xd0\xbe\xd1\x81\xd1\x82\xd0\xbe\xd0\xb9'), (2, b'\xd0\xa1\xd1\x80\xd0\xb5\xd0\xb4\xd0\xbd\xd0\xb8\xd0\xb9'), (3, b'\xd0\xa1\xd0\xbb\xd0\xbe\xd0\xb6\xd0\xbd\xd1\x8b\xd0\xb9')])),
                ('material_part', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name=b'\xd0\xa0\xd0\xb0\xd0\xb7\xd0\xb4\xd0\xb5\xd0\xbb \xd1\x83\xd1\x87\xd0\xb5\xd0\xb1\xd0\xbd\xd0\xbe\xd0\xb3\xd0\xbe \xd0\xbc\xd0\xb0\xd1\x82\xd0\xb5\xd1\x80\xd0\xb8\xd0\xb0\xd0\xbb\xd0\xb0', blank=True, to='organization.EducationFiles', null=True)),
                ('test', models.ForeignKey(verbose_name=b'\xd0\xa2\xd0\xb5\xd1\x81\xd1\x82', to='organization.EducationTest')),
            ],
            options={
                'verbose_name': '\u0412\u043e\u043f\u0440\u043e\u0441',
                'verbose_name_plural': '\u0412\u043e\u043f\u0440\u043e\u0441\u044b',
            },
        ),
        migrations.CreateModel(
            name='TestSessions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('session_id', models.CharField(max_length=255, verbose_name=b'Session ID')),
                ('answer_correct', models.BooleanField(default=False, verbose_name=b'\xd0\x9f\xd1\x80\xd0\xb0\xd0\xb2\xd0\xb8\xd0\xbb\xd1\x8c\xd0\xbd\xd1\x8b\xd0\xb9 \xd0\xbe\xd1\x82\xd0\xb2\xd0\xb5\xd1\x82')),
                ('question_start', models.DateTimeField(auto_now_add=True, verbose_name=b'\xd0\x9a\xd0\xbe\xd0\xb3\xd0\xb4\xd0\xb0 \xd0\xb1\xd1\x8b\xd0\xbb \xd0\xb7\xd0\xb0\xd0\xb4\xd0\xb0\xd0\xbd \xd0\xb2\xd0\xbe\xd0\xbf\xd1\x80\xd0\xbe\xd1\x81')),
                ('question_finish', models.DateTimeField(auto_now=True, verbose_name=b'\xd0\x9a\xd0\xbe\xd0\xb3\xd0\xb4\xd0\xb0 \xd0\xb1\xd1\x8b\xd0\xbb \xd0\xb4\xd0\xb0\xd0\xbd \xd0\xbe\xd1\x82\xd0\xb2\xd0\xb5\xd1\x82')),
                ('timing_correct', models.BooleanField(default=False, verbose_name=b'\xd0\xa3\xd0\xbb\xd0\xbe\xd0\xb6\xd0\xb8\xd0\xbb\xd1\x81\xd1\x8f \xd0\xb2\xd0\xbe \xd0\xb2\xd1\x80\xd0\xb5\xd0\xbc\xd0\xb5\xd0\xbd\xd0\xb8')),
                ('finish', models.BooleanField(default=False, verbose_name=b'\xd0\xa2\xd0\xb5\xd1\x81\xd1\x82 \xd0\xb7\xd0\xb0\xd0\xb2\xd0\xb5\xd1\x80\xd1\x88\xd0\xb5\xd0\xbd')),
                ('force_finish', models.BooleanField(default=False, verbose_name=b'\xd0\x90\xd0\xb2\xd0\xb0\xd1\x80\xd0\xb8\xd0\xb9\xd0\xbd\xd0\xbe\xd0\xb5 \xd0\xb7\xd0\xb0\xd0\xb2\xd0\xb5\xd1\x80\xd1\x88\xd0\xb5\xd0\xbd\xd0\xb8\xd0\xb5')),
                ('rating', models.IntegerField(default=0, verbose_name=b'\xd0\x9e\xd1\x86\xd0\xb5\xd0\xbd\xd0\xba\xd0\xb0')),
                ('percent', models.DecimalField(default=0, verbose_name=b'\xd0\x9f\xd1\x80\xd0\xbe\xd1\x86\xd0\xb5\xd0\xbd\xd1\x82', max_digits=5, decimal_places=2)),
                ('answer', models.ForeignKey(verbose_name=b'\xd0\x9e\xd1\x82\xd0\xb2\xd0\xb5\xd1\x82', blank=True, to='organization.QuestionAnswer', null=True)),
                ('question', models.ForeignKey(verbose_name=b'\xd0\x92\xd0\xbe\xd0\xbf\xd1\x80\xd0\xbe\xd1\x81', to='organization.TestQuestion')),
                ('test', models.ForeignKey(verbose_name=b'\xd0\xa2\xd0\xb5\xd1\x81\xd1\x82', to='organization.EducationTest')),
                ('user', models.ForeignKey(verbose_name=b'\xd0\x9f\xd0\xbe\xd0\xbb\xd1\x8c\xd0\xb7\xd0\xbe\xd0\xb2\xd0\xb0\xd1\x82\xd0\xb5\xd0\xbb\xd1\x8c', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-question_finish'],
            },
        ),
        migrations.CreateModel(
            name='University',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 \xd0\x92\xd0\xa3\xd0\x97\xd0\xb0')),
                ('full_name', models.CharField(max_length=255, verbose_name=b'\xd0\x9f\xd0\xbe\xd0\xbb\xd0\xbd\xd0\xbe\xd0\xb5 \xd0\xbd\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 \xd0\x92\xd0\xa3\xd0\x97\xd0\xb0')),
            ],
            options={
                'verbose_name': '\u0412\u0423\u0417',
                'verbose_name_plural': '\u0412\u0423\u0417\u044b',
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(default=1, max_length=50, verbose_name=b'\xd0\xa1\xd1\x82\xd0\xb0\xd1\x82\xd1\x83\xd1\x81 \xd0\xbf\xd0\xbe\xd0\xbb\xd1\x8c\xd0\xb7\xd0\xbe\xd0\xb2\xd0\xb0\xd1\x82\xd0\xb5\xd0\xbb\xd1\x8f', choices=[(1, b'\xd0\xa1\xd1\x82\xd1\x83\xd0\xb4\xd0\xb5\xd0\xbd\xd1\x82'), (2, b'\xd0\x9f\xd1\x80\xd0\xb5\xd0\xbf\xd0\xbe\xd0\xb4\xd0\xb0\xd0\xb2\xd0\xb0\xd1\x82\xd0\xb5\xd0\xbb\xd1\x8c')])),
                ('middle_name', models.CharField(max_length=255, verbose_name=b'\xd0\x9e\xd1\x82\xd1\x87\xd0\xb5\xd1\x81\xd1\x82\xd0\xb2\xd0\xbe', blank=True)),
                ('department', models.ForeignKey(verbose_name=b'\xd0\x9a\xd0\xb0\xd1\x84\xd0\xb5\xd0\xb4\xd1\x80\xd0\xb0', to='organization.Department', null=True)),
                ('university', models.ForeignKey(verbose_name=b'\xd0\x92\xd0\xa3\xd0\x97', to='organization.University', null=True)),
                ('user', models.OneToOneField(related_name='profile', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='questionanswer',
            name='question',
            field=models.ForeignKey(verbose_name=b'\xd0\x92\xd0\xbe\xd0\xbf\xd1\x80\xd0\xbe\xd1\x81', to='organization.TestQuestion'),
        ),
        migrations.AddField(
            model_name='educationtest',
            name='subject',
            field=models.ForeignKey(verbose_name=b'\xd0\x94\xd0\xb8\xd1\x81\xd1\x86\xd0\xb8\xd0\xbf\xd0\xbb\xd0\xb8\xd0\xbd\xd0\xb0', to='organization.Subject'),
        ),
        migrations.AddField(
            model_name='educationtest',
            name='university',
            field=models.ForeignKey(verbose_name=b'\xd0\x92\xd0\xa3\xd0\x97', to='organization.University'),
        ),
        migrations.AddField(
            model_name='educationmaterial',
            name='subject',
            field=models.ForeignKey(verbose_name=b'\xd0\x94\xd0\xb8\xd1\x81\xd1\x86\xd0\xb8\xd0\xbf\xd0\xbb\xd0\xb8\xd0\xbd\xd0\xb0', to='organization.Subject'),
        ),
        migrations.AddField(
            model_name='educationmaterial',
            name='university',
            field=models.ForeignKey(verbose_name=b'\xd0\x92\xd0\xa3\xd0\x97', to='organization.University'),
        ),
        migrations.AddField(
            model_name='educationfiles',
            name='material',
            field=models.ForeignKey(verbose_name=b'\xd0\x9c\xd0\xb0\xd1\x82\xd0\xb5\xd1\x80\xd0\xb8\xd0\xb0\xd0\xbb', to='organization.EducationMaterial'),
        ),
        migrations.AddField(
            model_name='department',
            name='university',
            field=models.ForeignKey(verbose_name=b'\xd0\x92\xd0\xa3\xd0\x97', to='organization.University'),
        ),
    ]
