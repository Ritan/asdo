# -*- coding: utf-8 -*-
import os
from django.db import models
from django.contrib.auth.models import User, Group
from django.dispatch import receiver
from ckeditor.fields import RichTextField
from django.conf import settings
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

class University(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название ВУЗа')
    full_name = models.CharField(max_length=255, verbose_name='Полное название ВУЗа')

    class Meta:
        verbose_name = 'ВУЗ'
        verbose_name_plural = 'ВУЗы'

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return 'university/%s/' % self.pk


class Department(models.Model):
    university = models.ForeignKey(University, verbose_name='ВУЗ')
    name = models.CharField(max_length=255, verbose_name='Название кафедры')
    full_name = models.CharField(max_length=255, verbose_name='Полное название кафедры')

    class Meta:
        verbose_name = 'Кафедра'
        verbose_name_plural = 'Кафедры'

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return 'department/%s/' % self.pk


class Subject(models.Model):
    department = models.ForeignKey(Department, verbose_name='Кафедра')
    name = models.CharField(max_length=255, verbose_name='Название дисциплины')
    full_name = models.CharField(max_length=255, verbose_name='Полное название дисциплины')

    class Meta:
        verbose_name = 'Дисциплина'
        verbose_name_plural = 'Дисциплины'

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return 'subject/%s/' % self.pk


class UserProfile(models.Model):
    # пользователя ни чем не ограничиваем может смотреть все за исключением преподов, которые видят только свои материалы
    user = models.OneToOneField(User,related_name="profile")
    university = models.ForeignKey(University, verbose_name='ВУЗ', null=True) # основной вуз
    department = models.ForeignKey(Department, verbose_name='Кафедра', null=True) # основная кафедра
    status = models.ForeignKey(Group, verbose_name='Статус пользователя', null=True) #Статус теперь привязан к основной группе
    middle_name = models.CharField(max_length=255, verbose_name='Отчество', blank=True)

    def create_profile(sender, **kwargs):
        if kwargs['created']:
            UserProfile.objects.create(user=kwargs['instance'])
    models.signals.post_save.connect(create_profile, sender=User)

    def get_full_name(self):
        full_name = '%s %s' % (self.user.last_name, self.user.first_name)
        if self.middle_name:
            full_name += ' %s' % self.middle_name
        return full_name

    def get_name(self):
        print self.user.first_name[:1]
        name = '%s %s.' % (self.user.last_name, self.user.first_name[:1])
        if self.middle_name:
            name += ' %s.' % self.middle_name[:1]
        return name

class EducationMaterial(models.Model):
    author = models.ForeignKey(User, verbose_name='Автор (преподаватель)')
    subject = models.ForeignKey(Subject, verbose_name='Дисциплина')
    department = models.ForeignKey(Department, verbose_name='Кафедра')
    university = models.ForeignKey(University, verbose_name='ВУЗ')
    name = models.CharField(max_length=255, verbose_name='Название материала')
    description = models.TextField(verbose_name='Описание', blank=True)
    last_update_dt = models.DateTimeField(verbose_name='Дата последнего изменения', auto_now=True)
    create_dt = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)
    pub = models.BooleanField(verbose_name='Опубликовать', default=True)


    class Meta:
        verbose_name = 'Учебный материал'
        verbose_name_plural = 'Учебные материалы'

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return '/material/university/%s/department/%s/subject/%s/lector/%s/%s/' % (self.university.id, self.department.id, self.subject.id, self.author.id, self.pk)

class EducationFiles(models.Model):
    material = models.ForeignKey(EducationMaterial, verbose_name='Материал')
    name = models.CharField(max_length=255, verbose_name='Раздел курса')
    file = models.FileField(upload_to='materials', verbose_name='Файл')
    upload_dt = models.DateField(verbose_name='Дата загрузки', auto_now_add=True)

    def __unicode__(self):
        return '%s - %s' % (self.material.name, self.name)

    def get_absolute_url(self):
        return '%s%s' % (settings.MEDIA_URL, self.file)

# These two auto-delete files from filesystem when they are unneeded:
@receiver(models.signals.post_delete, sender=EducationFiles)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)

@receiver(models.signals.pre_save, sender=EducationFiles)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file = EducationFiles.objects.get(pk=instance.pk).file
    except EducationFiles.DoesNotExist:
        return False


class EducationTest(models.Model):
    author = models.ForeignKey(User, verbose_name='Автор (преподаватель)')
    subject = models.ForeignKey(Subject, verbose_name='Дисциплина')
    department = models.ForeignKey(Department, verbose_name='Кафедра')
    university = models.ForeignKey(University, verbose_name='ВУЗ')
    name = models.CharField(max_length=255, verbose_name='Название теста')
    description = models.TextField(verbose_name='Описание', blank=True)
    material = models.ForeignKey(EducationMaterial, verbose_name='Учебный материал', on_delete=models.SET_NULL, null=True, blank=True)
    last_update_dt = models.DateTimeField(verbose_name='Дата последнего изменения', auto_now=True)
    create_dt = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)
    pub = models.ManyToManyField(UserProfile, verbose_name='Опубликовать')
    level_up = models.IntegerField(verbose_name='Количество правильныйх ответов для повышения уровня сложности вопроса', default=0)
    level_down = models.IntegerField(verbose_name='Количество неправильных ответов для понижения уровня сложности вопроса', default=0)

    # градация оценки
    fail = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Меньше', help_text='Оценка 2')
    not_fail = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Меньше', help_text='Оценка 3')
    good = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Меньше', help_text='Оценка 4')
    # excellent = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Больше', help_text='Оценка 5')
    # меньше 33,33% - это оценка 2
    # от 33,33% до 44,44% - это оценка 3
    # от 44,44% до 66,66% - это оценка 4
    # и все что выше 66,66% это пять

    def admin_questions(self):
        return '<a href="/admin/organization/testquestion/?test__id__exact=%s">Вопросы</a>' % self.id
    admin_questions.allow_tags = True
    admin_questions.short_description = u'Вопросы'

    class Meta:
        verbose_name = 'Тест'
        verbose_name_plural = 'Тесты'

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return '/test/university/%s/department/%s/subject/%s/lector/%s/%s/' % (self.university.id, self.department.id, self.subject.id, self.author.id, self.pk)

    def get_questions(self):
        return TestQuestion.objects.filter(test=self)

    def get_simple_questions(self):
        return self.get_questions().filter(complexity=1)

    def get_normal_questions(self):
        return self.get_questions().filter(complexity=2)

    def get_difficult_questions(self):
        return self.get_questions().filter(complexity=3)



COMPLEXITY_CHOICES = ((1, 'Простой'),(2, 'Средний'),(3, 'Сложный'))

class TestQuestion(models.Model):
    test = models.ForeignKey(EducationTest, verbose_name='Тест')
    number = models.IntegerField(verbose_name='Номер вопроса', default=0)
    question = RichTextField(verbose_name='Вопрос')
    description = models.TextField(verbose_name='Описание', blank=True)
    material_part = models.ForeignKey(EducationFiles, verbose_name='Раздел учебного материала', on_delete=models.SET_NULL, null=True, blank=True)
    last_update_dt = models.DateTimeField(verbose_name='Дата последнего изменения', auto_now=True)
    create_dt = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)
    pub = models.BooleanField(verbose_name='Опубликовать', default=False)
    time = models.IntegerField(verbose_name='Время ответа на вопрос (мин)', default=1)
    complexity = models.IntegerField(verbose_name='Сложность вопроса', choices=COMPLEXITY_CHOICES, default=COMPLEXITY_CHOICES[0][0])

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'

    def __unicode__(self):
        return self.question

    def get_absolute_url(self):
        return '/test/%s/question/%s/' % (self.test.id, self.pk)

class QuestionAnswer(models.Model):
    question = models.ForeignKey(TestQuestion, verbose_name='Вопрос')
    answer = models.CharField(max_length=255, verbose_name='Вариант ответа')
    correct = models.BooleanField(default=False, verbose_name='Правильный ответ')

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'

    def __unicode__(self):
        return self.answer


class TestSessions(models.Model):
    session_id = models.CharField(max_length=255, verbose_name='Session ID')
    user = models.ForeignKey(User, verbose_name='Пользователь')
    test = models.ForeignKey(EducationTest, verbose_name='Тест')
    question = models.ForeignKey(TestQuestion, verbose_name='Вопрос')
    answer = models.ForeignKey(QuestionAnswer, verbose_name='Ответ', blank=True, null=True)

    answer_correct = models.BooleanField(verbose_name='Правильный ответ', default=False)

    question_start = models.DateTimeField(verbose_name='Когда был задан вопрос', auto_now_add=True)
    question_finish = models.DateTimeField(verbose_name='Когда был дан ответ', auto_now=True)
    timing_correct = models.BooleanField(verbose_name='Уложился во времени', default=False)

    finish = models.BooleanField(verbose_name='Тест завершен', default=False)
    force_finish = models.BooleanField(verbose_name='Аварийное завершение', default=False)

    rating = models.IntegerField(verbose_name='Оценка', default=0)
    percent = models.DecimalField(verbose_name='Процент', max_digits=5, decimal_places=2, default=0)

    def get_exist_questions(self):
        return TestSessions.objects.filter(test=self.test, session_id=self.session_id, user=self.user)

    def get_session_start(self):
        if self.finish:
            first_q = self.get_exist_questions().order_by('question_start')[0]
            return first_q.question_start

    def get_not_correct_answers(self):
        return self.get_exist_questions().filter(answer_correct=False)

    def __str__(self):
        return "id: {}\nName: {}\nQuestion: {}".format(self.session_id, self.user, self.question)


    class Meta:
        ordering = ['-question_finish']
        verbose_name = 'Сессия тестировая'
        verbose_name_plural = 'Сессии тестирования'




