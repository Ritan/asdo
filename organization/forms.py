# -*- coding:utf-8 -*-
import re
from django import forms
from django.contrib.auth.models import User, Group
from django.core.urlresolvers import reverse_lazy
from organization.models import UserProfile, University, Department, EducationMaterial, EducationFiles, \
    TestQuestion
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm
from django.conf import settings

class UserAuthenticationForm(AuthenticationForm):

    def __init__(self, *args, **kwargs):
        super(UserAuthenticationForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = forms.TextInput(attrs={'class': 'form-control'})
        self.fields['username'].label = u'Логин'
        self.fields['password'].widget = forms.PasswordInput(attrs={'class': 'form-control'})

class UserPasswordResetForm(PasswordResetForm):

    def __init__(self, *args, **kwargs):
        super(UserPasswordResetForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget = forms.TextInput(attrs={'class': 'form-control'})



class UserRegistrationForm(forms.Form):
    university = forms.ModelChoiceField(
        label = 'Выбор ВУЗа',
        queryset = University.objects.all(),
        # initial=0,
        error_messages = {'required': 'Укажите ВУЗ'},
        widget = forms.Select(attrs={'class': 'form-control'})
    )
    department = forms.ModelChoiceField(
        label = 'Выбор кафедры',
        queryset = Department.objects.all(),
        error_messages = {'required': 'Выберите кафедру'},
        widget = forms.Select(attrs={'class': 'form-control'})
    )
    username = forms.CharField(
        label = 'Логин',
        max_length = 30,
        error_messages = {'required': 'Укажите логин', 'max_length': 'Макс. длина логина 30 символов'},
        widget = forms.TextInput(attrs={'class': 'form-control'})
    )
    password = forms.CharField(
        label = 'Пароль',
        max_length = 30,
        error_messages = {'required': 'Укажите пароль', 'max_length': 'Макс. длина пароля 30 символов'},
        widget = forms.PasswordInput(attrs={'class': 'form-control'})
    )
    email = forms.EmailField(
        label = 'Email',
        error_messages = {'required': 'Укажите Email', 'invalid':'Неверно указан email'},
        widget = forms.EmailInput(attrs={'class': 'form-control'})
    )
    last_name = forms.CharField(
        label = 'Фамилия',
        error_messages = {'required': 'Укажите Фамилию'},
        widget = forms.TextInput(attrs={'class': 'form-control'})
    )
    first_name = forms.CharField(
        label = 'Имя',
        error_messages = {'required': 'Укажите Имя'},
        widget = forms.TextInput(attrs={'class': 'form-control'})
    )
    middle_name = forms.CharField(
        label = 'Отчество',
        required = False,
        error_messages = {'required': 'Укажите Отчество'},
        widget = forms.TextInput(attrs={'class': 'form-control'})
    )
    # status = forms.ChoiceField(
        # label = 'Статус',
        # choices = USER_STATUS,
        # initial = USER_STATUS[0][0],
        # widget = forms.Select(attrs={'class': 'form-control'})
    # )
    group = forms.ModelChoiceField(
        label = 'Статус',
        queryset = Group.objects.all(),
        error_messages = {'required': 'Выберите ваш статус'},
        widget = forms.Select(attrs={'class': 'form-control'})
    )

    def clean(self):
        cleaned_data = super(UserRegistrationForm, self).clean()

        if cleaned_data.get('username'):
            if re.match(ur'^[\w.@+-]+$', cleaned_data.get('username')) is None:
                msg = u'Неверный формат логина, используйте латинские символы'
                self._errors['username'] = self.error_class([msg])
            if User.objects.filter(username=cleaned_data.get('username')):
                msg = u'Пользователь с таким логином уже существует'
                self._errors['username'] = self.error_class([msg])


        return cleaned_data

    def save(self):
        data = self.cleaned_data
        print data['username']
        users = User.objects
        group = data['group']
        user = User.objects.create_user(
            username = data['username'],
            email = data['email'],
            password = data['password'],
            first_name = data['first_name'],
            last_name = data['last_name'],
        )
        user.groups.add(group.id)

        user.save()
        msg = u'Здравствуйте, %s!\n\n' % user.get_full_name()
        msg += u'Вы зарегистрировались в Автоматизированной Системе Дистанционного Обучения.\n Для доступа к ней используйте следующие данные: \n\n'
        msg += u'Логин: %s\n' % user.username
        msg += u'Пароль: %s\n' % data['password']
        # msg += u'\nКак только администратор активирует ваш аккаунт и вы получите уведомление на этот email, вы сможете продолжить пользоваться системой.'
        # user.email_user(subject=u'Регистрация в АСДО', message=msg, from_email=settings.ADMIN_EMAIL)


        profile = UserProfile.objects.get(user=user)
        profile.user = user
        profile.university = data['university']
        profile.department = data['department']
        profile.status = group
        profile.middle_name = data['middle_name']
        profile.save()
        print 'UserProfile: ', profile
        return user



class AddEducationMaterialForm(forms.Form):
    university = forms.ModelChoiceField(
        queryset=University.objects.all(),
        required=True,
        empty_label=u'Выберите ВУЗ',
        label=u'Университет',
        widget=forms.Select(attrs={'class':'form-control'})
    )
    department = forms.ChoiceField(
        required=True,
        label=u'Кафедра',
        widget=forms.Select(attrs={'class':'form-control'})
    )
    subject = forms.ChoiceField(
        required=True,
        label=u'Дисциплина',
        widget=forms.Select(attrs={'class':'form-control'})
    )
    name = forms.CharField(
        required=True,
        label=u'Название',
        widget=forms.TextInput(attrs={'class':'form-control'})
    )
    description = forms.CharField(
        required=True,
        label=u'Описание',
        widget=forms.Textarea(attrs={'class':'form-control', 'rows':'3'})
    )


class AddEducationFilesForm(forms.ModelForm):
    class Meta:
        model = EducationFiles
        fields = ('name', 'file')
        widgets = {
            'material': forms.HiddenInput(),
            'name': forms.TextInput(attrs={'class':'form-control'}),
            'file': forms.FileInput(attrs={
                'class':'file',
                'data-show-preview':'false',
                'data-show-upload':'false',
                'data-browse-label':'',
                'data-remove-label':'',
            }),
        }

class AddTestQuestionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        material_id = kwargs.pop('materialid')
        super(AddTestQuestionForm, self).__init__(*args, **kwargs)
        self.fields['material_part'] = forms.ModelChoiceField(
                required=False,
                queryset=EducationFiles.objects.filter(material=material_id),
                label='Раздел учебного материала',
                widget=forms.Select(attrs={'class':'form-control'}))

    class Meta:
        model = TestQuestion
        fields= ('test', 'question', 'material_part', 'time', 'complexity')
        widgets = {
            'test': forms.HiddenInput(),
            'number': forms.TextInput(attrs={'class':'form-control'}),
            'question': forms.Textarea(attrs={'class':'form-control'}),
            'material_part': forms.Select(attrs={'class':'form-control'}),
            'time': forms.TextInput(attrs={'class':'form-control'}),
            'complexity': forms.Select(attrs={'class':'form-control'}),
        }

class AddEducationTestForm(forms.Form):
    university = forms.ModelChoiceField(
        queryset=University.objects.all(),
        required=True,
        empty_label=u'Выберите ВУЗ',
        label=u'Университет',
        widget=forms.Select(attrs={'class':'form-control'})
    )
    department = forms.ChoiceField(
        required=True,
        label=u'Кафедра',
        widget=forms.Select(attrs={'class':'form-control'})
    )
    subject = forms.ChoiceField(
        required=True,
        label=u'Дисциплина',
        widget=forms.Select(attrs={'class':'form-control'})
    )
    material = forms.ModelChoiceField(
        queryset=EducationMaterial.objects.all(),
        required=False,
        empty_label=u'Выберите Учебный материал',
        label=u'Учебный материал',
        widget=forms.Select(attrs={'class':'form-control'})
    )
    # material = forms.ChoiceField(
    #     required=True,
    #     label=u'Учебный материал',
    #     widget=forms.Select(attrs={'class':'form-control'})
    # )
    name = forms.CharField(
        required=True,
        label=u'Название',
        widget=forms.TextInput(attrs={'class':'form-control'})
    )
    description = forms.CharField(
        required=True,
        label=u'Описание',
        widget=forms.Textarea(attrs={'class':'form-control', 'rows':'3'})
    )
    level_up = forms.CharField(
        required=True,
        label=u'Количество правильных ответов для повышения уровня сложности вопроса',
        widget=forms.TextInput(attrs={'class':'form-control'})
    )
    level_down = forms.CharField(
        required=True,
        label=u'Количество неправильных ответов для понижения уровня сложности вопроса',
        widget=forms.TextInput(attrs={'class':'form-control'})
    )
    fail = forms.CharField(
        required=True,
        label=u'Оценка 3',
        help_text='Оценка 3',
        widget=forms.TextInput(attrs={'class':'form-control'})
    )
    not_fail = forms.CharField(
        required=True,
        label=u'Оценка 4',
        help_text='Оценка 4',
        widget=forms.TextInput(attrs={'class':'form-control'})
    )
    good = forms.CharField(
        required=True,
        label=u'Оценка 5',
        help_text='Оценка 5',
        widget=forms.TextInput(attrs={'class':'form-control'})
    )
    # excellent = forms.CharField(
    #     required=True,
    #     label=u'Оценка 5',
    #     help_text='Оценка 5',
    #     widget=forms.TextInput(attrs={'class':'form-control'})
    # )

