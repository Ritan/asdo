# -*- coding: utf-8 -*-
from django.contrib.auth import authenticate, login
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from forms import *
from views import home

def registration(request):
    form = UserRegistrationForm(request.POST or None)
    print form
    context = {'form': form}
    if request.method == "POST" and form.is_valid():
        user = form.save()
        user = authenticate(username = user.username, password = form.cleaned_data.get('password'))
        if user is not None:
             login(request, user)
        return HttpResponseRedirect(reverse('home'))
    return render(request, 'registration/registration.html', context)


def registration_success(request):
    context = {'success':True}
    return render(request, 'registration/registration.html', context)
